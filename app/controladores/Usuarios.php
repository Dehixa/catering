<?php
	//Para cargar todos lo archivos dentro de librerias/*
	spl_autoload_register(function($nombreClase){
		require_once '../modelos/'.$nombreClase.'.php';
	});

 	require_once '../modelos/modeloUsuario.php';
 	$metodo=0;
 	//la variable metodo identificara la accion del controlador ya sea registrar, editar, mostrar etc

 	# Recibimos los datos leídos de php://input
	$datosRecibidos = file_get_contents("php://input");
	# No los hemos decodificado, así que lo hacemos de una vez:
	$getDatos = json_decode($datosRecibidos);
	# Ahora podemos acceder a los datos
	$metodo = $getDatos->metodo;

 	$usuario = new UsuarioModelo();

 	// mostrar datos
	if($metodo==1){
 		$json_mostrar = $usuario->mostrarRegistros();
 		echo json_encode($json_mostrar,JSON_FORCE_OBJECT);
 	}

 	// Registrar cliente
 	if($metodo==2){
 		$json_registrar = $usuario->agregarUsuario($datos);
 		echo json_encode($json_registrar);
 		/*
 		$string = JSON.stringify($json_registration);
 		$string = strrev($string);
 		$string = substr($string, 20);
 		$string = strrev($string)."}";
 		echo json_decode($string);*/
 	}

 	// Registrar proveedor
 	if($metodo==3){
 		$tipoUsuario = 2;
 		$razonSocial = $getDatos->razonSocial;
 		$ruc = $getDatos->ruc;
 		$apePat = $getDatos->apePat;
 		$apeMat = $getDatos->apeMat;
 		$nombres = $getDatos->nombres;
 		$direccion = $getDatos->direccion;
 		$distrito = $getDatos->distrito;
 		$provincia = $getDatos->provincia;
 		$departamento = $getDatos->departamento;
 		$paginaWeb = $getDatos->paginaWeb;
 		$telefono = $getDatos->telefono;
 		$email = $getDatos->email;
 		$user= $getDatos->email;
 		$password = $getDatos->password;

 		$datos=[$tipoUsuario, $razonSocial, $ruc, $apePat, $apeMat, $nombres, $direccion, $distrito, $provincia, $departamento, $paginaWeb, $telefono, $email, $user, $password];

 		$json_registrar = $usuario->agregarProveedor($datos);
 		echo json_encode($json_registrar);
 		/*
 		$string = JSON.stringify($json_registration);
 		$string = strrev($string);
 		$string = substr($string, 20);
 		$string = strrev($string)."}";
 		echo json_decode($string);*/
 	}


 	// Login
 	if(!empty($username) && !empty($password) && empty($email)){
 		$hashed_password = md5($password);
 		$json_array = $usuario->loginUsers($username, $hashed_password);
 		echo json_encode($json_array);
 	}
 ?>
