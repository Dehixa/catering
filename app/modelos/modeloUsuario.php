<?php

include_once '../librerias/db-connect.php';

class UsuarioModelo{
	private $db;
	private $db_table = "usuario";
	public function __construct(){
		$this->db = new DbConnect();

	}
	

	public function mostrarRegistros(){
		$query = "SELECT * from ".$this->db_table;
		$result = mysqli_query($this->db->getDb(),$query);
		if(mysqli_num_rows($result) > 0){
			$json = array();
			$i=0;
 			while($row = mysqli_fetch_assoc($result)){
 			  				 				
				$json['estudiantes'][]=$row;
			 			}
			
 			mysqli_close($this->db->getDb());
			return $json;
 		}else{
		
		mysqli_close($this->db->getDb());
		return false;}
	}


	

	public function agregarUsuario($datos){	
		$json = array();
		$query = "INSERT INTO ".$this->db_table."(UsuUsu,UsuPas,UsuCorEle,UsuEstReg) VALUES ('$datos[0]', '$datos[1]', '$datos[2]',16)";
		$inserted = mysqli_query($this->db->getDb(), $query);
		if($inserted == 1){
			$json['success'] = 1;
			$json['message'] = "Cliente registrado con exito";
		}else{
			$json['success'] = 0;
			$json['message'] = "Error al registrar usuario";
		}
		mysqli_close($this->db->getDb());
			
		
		return $json;
	}

	public function agregarProveedor($datos){	
		$json = array();
		$query = "INSERT INTO ".$this->db_table."(UsuTipUsu, UsuRazSoc, UsuRUC, UsuApePat, UsuApeMat, UsuNom, UsuDir, UsuDis, UsuPro, UsuDep, UsuPagWeb, UsuTel, UsuCorEle, UsuUsu, UsuPass, UsuEstReg) VALUES ('$datos[0]', '$datos[1]', '$datos[2]', '$datos[3]', '$datos[4]', '$datos[5]', '$datos[6]', '$datos[7]', '$datos[8]', '$datos[9]', '$datos[10]', '$datos[11]', '$datos[12]', '$datos[13]', '$datos[14]', 16)";

		$inserted = mysqli_query($this->db->getDb(), $query);
		if($inserted == 1){
			$json['success'] = 1;
			$json['message'] = "Proveedor registrado con exito";
		}else{
			$json['success'] = 0;
			$json['message'] = "Error al registrar usuario";
		}
		mysqli_close($this->db->getDb());
			
		
		return $json;
	}

}
?>